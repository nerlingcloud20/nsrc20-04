<?php

namespace App\Repository;

use App\Entity\LoginData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LoginData|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoginData|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoginData[]    findAll()
 * @method LoginData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoginDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LoginData::class);
    }

    // /**
    //  * @return LoginData[] Returns an array of LoginData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LoginData
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
