<?php

namespace App\Entity;

use App\Repository\LoginDataRepository;
use Doctrine\ORM\Mapping as ORM;


class LoginData
{
    private $email;
    private $password;
    private $tfa;


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getTfa(): ?string
    {
        return $this->tfa;
    }

    public function setTfa(?string $tfa): self
    {
        $this->tfa = $tfa;

        return $this;
    }
}
