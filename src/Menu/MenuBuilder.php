<?php
namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;

class MenuBuilder
{
    private $factory;

    /**
     * Add any other dependency you need...
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Home', ['route' => 'root', 'class' => 'navbar-link']);
        $menu->addChild('Nerling-Homepage', ['uri' => 'https://www.nerling.de', 'class' => 'navbar-item has-dropdown']);
        $menu['Nerling-Homepage']->addChild('Impressum', ['uri' => 'https://www.nerling.de/impressum.html', 'class' => 'navbar-link']);
        $menu->setChildrenAttribute('class', 'menu-list');
// ... add more children

        return $menu;
    }
}