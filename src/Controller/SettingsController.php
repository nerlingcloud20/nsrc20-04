<?php

namespace App\Controller;

use App\DataClasses\MenuItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
;

class SettingsController extends AbstractController
{
    private HttpClientInterface $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }

    /**
     * @Route("/settings", name="app_settings")
     * @Route("/settings/account", name="app_settings_account")
     */
    public function index(Request $request): Response
    {
        if(CustomerController::isLoggedIn($request, $this->backendClient))
            return $this->render('settings/index.html.twig', [
            'projects' => MainFunctions::getProjectData(MainFunctions::getUserSessionFromRequest($request), $this->backendClient),
            'menu' => MainFunctions::generateMenuData($request,$this->backendClient),
        ]);
        else
            return CustomerController::notLoggedInResponse($this);
    }
}
