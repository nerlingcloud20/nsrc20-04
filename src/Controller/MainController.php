<?php

namespace App\Controller;

use App\DataClasses\MenuItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
;

class MainController extends AbstractController
{

    private HttpClientInterface $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }
    /**
     * @Route(
     *     "/",
     *     name="root",
     *     requirements={
     *         "_locale": "en|de",
     *     }
     * )
     */
    public function index(Request $request): Response
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
            'menu' => MainFunctions::generateMenuData($request, $this->backendClient),
        ]);
    }
}
