<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
;

class CustomerController extends AbstractController
{
    private HttpClientInterface $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }

    /**
     * @Route("/customers/customer/subordinates/{id}", name="customers_customer_subordinates", methods="GET")
     */
    public function getCustomerSubordinatesId(Request $request, int $id=-1): Response {
        return MainFunctions::respondGetDataFromBackend($request,$this->backendClient,'/api/customers/customer/subordinates'.($id>10 ? '/'.$id : ''));
    }

    /**
     * @Route("/customers/customer", name="customers_customer", methods="GET")
     */
    public function getCustomer(Request $request): Response {
        return MainFunctions::respondGetDataFromBackend($request,$this->backendClient,'/api/customers/customer',array('authenticated','tfa_requested','tfa_totpsecret','passwd'));
    }

    /**
     * @Route("/customers/customer/partial", name="customers_customer_partial", methods="PUT")
     */
    public function putCustomerPartial(Request $request): Response {
        return MainFunctions::respondPUTRequestToBackend($request,$this->backendClient,'/api/customers/customer/partial');
    }

    /**
     * @Route("/customers/possible_tfa", name="customers_possibleTfa", methods="GET")
     */
    public function getPossibleTFAs(Request $request): Response {
        return MainFunctions::respondGetDataFromBackend($request,$this->backendClient,'/api/customers/possible_tfa');
    }

    public static function isLoggedIn(Request $request, HttpClientInterface $backendClient) : bool {
         return MainFunctions::getDataFromBackend($request,$backendClient,'/api/customers/customer/is_logged_in',[],true)->httpStatusCode == 200;
    }
    public static function getRights(Request $request, HttpClientInterface $backendClient, string $project, bool $readRights=true): array {
        return MainFunctions::getDataFromBackend($request,$backendClient,'/api/customers/customer/rights/project/'.$project.($readRights ? '/read' : '/write'),[])->data;
    }
    public static function hasRights(Request $request, HttpClientInterface $backendClient, string $project, bool $readRights=true): bool {
        return count(self::getRights($request,$backendClient,$project,$readRights)) > 0;
    }
    public static function notLoggedInResponse(AbstractController $c) : Response {
        $response = $c->redirectToRoute('login');
        $c->addFlash('failure', new TranslatableMessage('login.invalid'));
        return $response;
    }

    /**
     * @Route("/account/logout", name="account_logout", methods="GET")
     */
    public function accountLogout(Request $request): Response {
        $response = $this->redirectToRoute('root');
        try {
            $serverResponse = $this->backendClient->request(
                'GET',
                '/api/customers/customer/logout',
                [
                    'body' => [],
                    'headers' => [
                        'user-session' => MainFunctions::getUserSessionFromRequest($request),
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ]
                ]
            );
            $this->addFlash('success', new TranslatableMessage('login.logout'));

        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
        }
        $response->headers->clearCookie('user-session');
        $response->headers->clearCookie('user-session1');
        $response->sendHeaders();
        return $response;
    }


}