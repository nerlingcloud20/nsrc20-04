<?php

namespace App\Controller;

use App\Form\LoginFormType;
use App\Form\LoginTfaFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
;

class LoginController extends AbstractController
{
    private $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }

    /**
     * @Route(
     *     "/login",
     *     name="login",
     * )
     */
    public function index(Request $request): Response
    {
        $defaultData = ['message' => 'Type your message here'];
        $form = $this->createForm(LoginFormType::class, null, ['attr' => ['class' => 'tile is-parent is-vertical is-6']]
        );

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();

            $serverResponse = $this->backendClient->request(
                'POST',
                '/auth',
                [
                    'body' => [
                        'username' => $data['email'],
                        'password' => $data['password'],
                        'login' => 'submit'
                    ]
                ]
            );
            return $this->handleLogin($serverResponse, 'projects_main','login', 'tfa');
        } else {
            $response = $this->render('login/login.html.twig', [
                'form' => $form->createView()
            ]);
            $response->headers->clearCookie('user-session');
            return $response;
        }
    }

    /**
     * @Route(
     *     "/login-automatic",
     *     name="login-automatic",
     * )
     */
    public function autoLogin(Request $request): Response
    {
        $username = $request->request->get('username','');
        $passwd = $request->request->get('password','');
        $view = $request->request->get('view','');
        $hideMenu = $request->request->get('hideMenu','false');

        $serverResponse = $this->backendClient->request(
            'POST',
            '/auth',
            [
                'body' => [
                    'username' => $username,
                    'password' => $passwd,
                    'login' => 'submit'
                ]
            ]
        );
        return $this->handleLogin($serverResponse, 'app_customview','login', 'tfa',false,['view' => $view, 'hideMenu' => $hideMenu==='true']);
    }

    /**
     * @Route(
     *     "/2fa",
     *     name="tfa",
     * )
     */
    public function tfa(Request $request): Response
    {
        $cookies = $request->cookies->get('user-session1');

        $form = $this->createForm(LoginTfaFormType::class, null, ['attr' => ['class' => 'tile is-parent is-vertical is-6']]
        );
        $form->get('username')->setData($request->cookies->get('username'));

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $serverResponse = $this->backendClient->request(
                'POST',
                '/auth_2fa',
                [
                    'body' => [
                        'username' => $data['username'],
                        'tfa' => $data['password'],
                        'login' => 'submit'
                    ],
                    'headers' => [
                        'user-session1' => $cookies
                    ]
                ]
            );
            return $this->handleLogin($serverResponse, 'projects_main','tfa', 'tfa');
        }
        return $this->render('login/login.html.twig', [
            'form' => $form->createView()
        ]);

    }


    public function handleLogin(ResponseInterface &$serverResponse, string $routeToSuccess, string $routeToFail, string $routeToTfa, bool $printSuccess = true, array $routeParameters = []): Response
    {
        try {
            $statusCode = $serverResponse->getStatusCode();

            if ($statusCode >= 200 && $statusCode < 300) {
                $headers = $serverResponse->getHeaders();

                //user-session is the fully authenticated session, others with index are used on the way
                $cookies = isset($headers['user-session']) ? ['fully-authenticated' => true, 'cookie-name' => 'user-session', 'cookie-value' => $headers['user-session'][0]] : null;
                $cookies = isset($headers['user-session1']) && $cookies === null ? ['fully-authenticated' => false, 'cookie-name' => 'user-session1', 'cookie-value' => $headers['user-session1'][0]] : $cookies;


                $response = null;
                if ($cookies != null) {

                    //Redirect directly to $routeToSuccess if the authentication is complete
                    $response = $this->redirectToRoute($cookies['fully-authenticated'] ? $routeToSuccess : $routeToTfa, $routeParameters);

                    $response->headers->setCookie(Cookie::create($cookies['cookie-name'], $cookies['cookie-value'])->withHttpOnly(false));
                    if (isset($headers['username']))
                        $response->headers->setCookie(Cookie::create('username', $headers['username'][0]));

                    // Clear out Outdated Cookies
                    if($cookies['fully-authenticated']) {
                        $response->headers->clearCookie('username');
                        $response->headers->clearCookie('user-session1');
                    }
                    if($printSuccess) $this->addFlash('success', new TranslatableMessage('Login.success'));
                    $response->sendHeaders();
                } else {
                    $this->addFlash('failure', new TranslatableMessage('login.invalid'));
                }

                //$response->sendHeaders();
                return $response;
            }
            if ($statusCode === 401) { // Unauthenticated
                $this->addFlash('failure', new TranslatableMessage('login.invalid'));
                return $this->redirectToRoute($routeToFail);
            }
            $this->addFlash('failure', $serverResponse->getContent(false));
            return $this->redirectToRoute($routeToFail);
        } catch (ClientExceptionInterface $e) {
            $this->addFlash('failure', $e->getCode());
            return $this->redirectToRoute($routeToFail);
        } catch (TransportExceptionInterface | RedirectionExceptionInterface $e) {
            $this->addFlash('failure', $e->getCode(). ' - Connection to server malfunctioning');
            return $this->redirectToRoute($routeToFail);
        } catch (ServerExceptionInterface $e) {
            $this->addFlash('failure', $e->getCode() . ' - Internal Server Error');
            return $this->redirectToRoute($routeToFail);
        }
    }


}
