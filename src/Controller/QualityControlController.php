<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\RedirectionException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
;

class QualityControlController extends AbstractController
{

    private HttpClientInterface $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }

    /**
     * @Route("/qc/rules/{id}", name="qc_rule", defaults={"id"=-1}, methods={"GET", "POST"})
     */
    public function index(Request $request, int $id): Response
    {
        if($request->isMethod('POST')) {
            return MainFunctions::respondPOSTRequestToBackend($request,$this->backendClient,'/api/qc/rules',array('id' => -1));
        }

        return $this->render('quality_control/index.html.twig', [
            'controller_name' => 'QualityControlController',
        ]);
    }

    /**
     * @Route("/qc/rules/project/{project}", name="qc_rule_project", methods="GET")
     */
    public function projectRules(Request $request, string $project): Response
    {
        return MainFunctions::respondGetDataFromBackend($request,$this->backendClient,'/api/qc/rules/project/'.$project);
/*
        try {
            $serverResponse = $this->backendClient->request(
                'GET',
                '/api/qc/rules/project/'.$project,
                [
                    'body' => [],
                    'headers' => [
                        'user-session' => MainFunctions::getUserSessionFromRequest($request)
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                case 204:
                    return new JsonResponse($serverResponse->toArray(true));

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
*/
    }

    /**
     * @Route("/qc/project/{project}", name="qc_project", methods="GET")
     */
    public function currentQc(Request $request, string $project): Response {
        return MainFunctions::respondGetDataFromBackend($request,$this->backendClient,'/api/qc/project/'.$project);

/*
        try {
            $serverResponse = $this->backendClient->request(
                'GET',
                '/api/qc/project/'.$project,
                [
                    'body' => [],
                    'headers' => [
                        'user-session' => MainFunctions::getUserSessionFromRequest($request)
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    return new JsonResponse($serverResponse->toArray(true),200);
                case 204:
                    return new JsonResponse(null,204);

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
*/
    }

    /**
     * HÄTTE SEIN SOLLEN, TOT WEIL GET SCHON MATCHT@Route("/qc/rules", name="qc_rules", methods="POST")
     */
    public function addRule(Request $request): Response {

        return MainFunctions::respondPOSTRequestToBackend($request,$this->backendClient,'/api/customers/customer/partial',array('id' => -1));

//        $cookies = $request->cookies->get('user-session') ?? $request->headers->get('user-session');
/*
        if($request->headers->get('Content-Type') === 'application/json') {
            $data = json_decode($request->getContent(), true);
            $api_object = array("id"=> -1,
                "project" => $data['project'] ?? "",
                "type" => $data['type'],
                "sensor1" => $data['sensor1'],
                "sensor2" => $data['sensor2'] ?? "",
                "value1" => $data['value1'] ?? 0.0, // Value for GKS, lower Value for Absolute
                "value2" => $data['value2'] ?? 0.0, // upper Value for Absolute
                "distance" => $data['distance'] ?? 0.0,
                "unit" => $data['unit'] ?? "",
                "active" => $data['active']);
        } else {
            $api_object = $request->request->all();
            $api_object['id'] = -1;
        }


        try {
            $serverResponse = $this->backendClient->request(
                'POST',
                '/api/qc/rules',
                [
                    'body' => json_encode($api_object),
                    'headers' => [
                        'user-session' => MainFunctions::getUserSessionFromRequest($request),
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    return new JsonResponse($serverResponse->toArray(true),200);
                case 204:
                    return new JsonResponse(null,204);

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
*/
    }
    /**
     * @Route("/qc/rules/{id}", name="qc_rules", methods="PUT")
     */
    public function modifyRule(Request $request, int $id): Response {
        $cookies = $request->cookies->get('user-session') ?? $request->headers->get('user-session');

        if($request->headers->get('Content-Type') === 'application/json') {
            $data = json_decode($request->getContent(), true);
            $api_object = array("id"=> $data['id'],
                "project" => $data['project'] ?? "",
                "type" => $data['type'],
                "sensor1" => $data['sensor1'],
                "sensor2" => $data['sensor2'] ?? "",
                "value1" => $data['value1'] ?? 0.0, // Value for GKS, lower Value for Absolute
                "value2" => $data['value2'] ?? 0.0, // upper Value for Absolute
                "distance" => $data['distance'] ?? 0.0,
                "unit" => $data['unit'] ?? "",
                "active" => $data['active']);
        } else {
            $api_object = $request->request->all();
        }


        try {
            $serverResponse = $this->backendClient->request(
                'PUT',
                '/api/qc/rules/'.$id,
                [
                    'body' => json_encode($api_object),
                    'headers' => [
                        'user-session' => MainFunctions::getUserSessionFromRequest($request),
                        'Content-Type' => 'application/json'
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    return new JsonResponse($serverResponse->toArray(true),200);
                case 204:
                    return new JsonResponse(null,204);

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
    }

    /**
     * @Route("/qc/possibleTypes", name="qc_rules_possibleTypes", methods="GET")
     */
    public function getPossibleTypes(Request $request): Response {
        try {
            $serverResponse = $this->backendClient->request(
                'GET',
                '/api/qc/possibleTypes',
                [
                    'body' => [],
                    'headers' => [
                        'user-session' => MainFunctions::getUserSessionFromRequest($request)
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                case 204:
                    return new JsonResponse($serverResponse->toArray(true));

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
    }
}
