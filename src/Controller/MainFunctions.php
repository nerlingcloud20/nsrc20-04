<?php

namespace App\Controller;

use App\DataClasses\GetDataResponse;
use App\DataClasses\MenuItem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class MainFunctions
{
    public static function generateMenuData(Request $request, HttpClientInterface $backendClient) : array
    {
        $projects = [];
        $user_session = MainFunctions::getUserSessionFromRequest($request);
        $loggedIn = CustomerController::isLoggedIn($request,$backendClient);
        foreach(self::getProjectData($user_session, $backendClient)  as &$value) {
            $projects[] = new MenuItem(strtoupper($value),"/projects/".$value,[],false);
        }

        if($loggedIn)
            $leftItems = array(
                0 => new MenuItem("menu.projects","/projects",$projects,false),
                1 => new MenuItem("menu.settings","/settings",[],false),
                2 => new MenuItem("menu.download","/download",[],false));
        else
            $leftItems = array();

        $rightItems = [];
        $login = array(
            'loggedIn' => $loggedIn,
            'children' => [
                0 => new MenuItem("menu.account.edit","/settings/account", [], false),
                1 => new MenuItem("menu.account.logout","/account/logout", [], false)]
        );
        return array(
            'left' => $leftItems,
            'right' => $rightItems,
            'login' => $login);
    }

    public static function getProjectData(string $user_session, HttpClientInterface $backendClient) : array {
        try {
            $serverResponse = $backendClient->request(
                'GET',
                '/api/customers/customer/rights',
                [
                    'body' => [],
                    'headers' => [
                        'user-session' => $user_session
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    return array_keys($serverResponse->toArray(true));
                default:
                    return [];
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return [];
        }
    }

    public static function getUserSessionFromRequest(Request $request) : string {
        return $request->cookies->get('user-session') ?? $request->headers->get('user-session') ?? '';
    }

    public static function respondGetDataFromBackend(Request $request, HttpClientInterface $backendClient, string $url, array $keysToKill = []) : Response {
        $response = self::getDataFromBackend($request,$backendClient,$url,$keysToKill);
        if($response->isValid) {
            return new JsonResponse($response->data,$response->httpStatusCode);
        } else {
            return new Response($response->dataRaw, $response->httpStatusCode);
        }
    }

    public static function getDataFromBackend(Request $request, HttpClientInterface $backendClient, string $url, array $keysToKill = [], bool $onlyStatusCode=false): GetDataResponse {
        try {
            $serverResponse = $backendClient->request(
                'GET',
                $url,
                [
                    'body' => [],
                    'headers' => [
                        'user-session' => self::getUserSessionFromRequest($request),
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    if($onlyStatusCode) return new GetDataResponse(200, [], true, $headers, "");

                    $data = $serverResponse->toArray(true);
                    foreach($keysToKill as &$key) {
                        if(isset($data[$key]))
                            unset($data[$key]);
                    }
                    return new GetDataResponse(200, $data, true, $headers, "");
                case 204:
                    return new GetDataResponse(204, [], true, $headers, "");

                default:
                    return new GetDataResponse($serverResponse->getStatusCode(), [], false, $headers, $serverResponse->getContent(false));
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new GetDataResponse(500,[],false,[],$e);
        }
    }

    public static function getDataArrayFromRequest(Request $request, array $overwriteData = null) : array {
        if(str_starts_with($request->headers->get('Content-Type'),'application/json')) {
            $data = json_decode($request->getContent(), true);
            $api_object = array();
            foreach($data as $key => &$value) {
                $api_object[$key] = $value;
            }
        } else {
            $api_object = $request->request->all();
        }
        if($overwriteData !== null) {
            foreach($overwriteData as $key => &$value) {
                $api_object[$key] = $value;
            }
        }

        return $api_object;
    }

    public static function respondPUTRequestToBackend(Request $request, HttpClientInterface $backendClient, string $url, array $overwriteData = null) : Response {
        return MainFunctions::respondPUTDataToBackend(self::getUserSessionFromRequest($request),$backendClient,$url,self::getDataArrayFromRequest($request,$overwriteData));
    }

    public static function respondPUTDataToBackend(string $user_session, HttpClientInterface $backendClient, string $url, array $data) : Response {
        try {
            $serverResponse = $backendClient->request(
                'PUT',
                $url,
                [
                    'headers' => [
                        'user-session' => $user_session,
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ],
                    'body' => json_encode($data)
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    return new JsonResponse($serverResponse->toArray(),200);
                case 204:
                    return new JsonResponse(null,204);

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
    }

    public static function respondPOSTRequestToBackend(Request $request, HttpClientInterface $backendClient, string $url, array $overwriteData = null) : Response {
        return MainFunctions::respondPOSTDataToBackend(self::getUserSessionFromRequest($request),$backendClient,$url,self::getDataArrayFromRequest($request,$overwriteData));
    }

    public static function respondPOSTDataToBackend(string $user_session, HttpClientInterface $backendClient, string $url, array $data) : Response {
        try {
            $serverResponse = $backendClient->request(
                'POST',
                $url,
                [
                    'headers' => [
                        'user-session' => $user_session,
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ],
                    'body' => json_encode($data)
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    return new JsonResponse($serverResponse->toArray(),200);
                case 204:
                    return new JsonResponse(null,204);

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
    }

    public static function uploadFile() {

    }
}