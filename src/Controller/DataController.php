<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Mime\MimeTypes;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
;

class DataController extends AbstractController
{
    private HttpClientInterface $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }

    /**
     * @Route("/data", name="app_data")
     * @Route("/download", name="app_download")
     */
    public function index(Request $request): Response
    {
        if (CustomerController::isLoggedIn($request, $this->backendClient))
            return $this->render('data/index.html.twig', [
                'projects' => MainFunctions::getProjectData(MainFunctions::getUserSessionFromRequest($request), $this->backendClient),
                'menu' => MainFunctions::generateMenuData($request, $this->backendClient),
            ]);
        else
            return CustomerController::notLoggedInResponse($this);
    }

    /**
     * @Route("/data/export/{project}/csv",name="app_data_export_project_csv", methods={"GET"})
     */
    public function createDownloadFile(Request $request, string $project): Response
    {
        //?days={days}&endtime={end}&subset={subset}
        $response = new JsonResponse([], 500);
        if (CustomerController::isLoggedIn($request, $this->backendClient)) {
            try {
                $serverResponse = $this->backendClient->request(
                    'GET',
                    '/api/data/export/' . $project . '/csv',
                    [
                        'body' => [],
                        'headers' => [
                            'user-session' => MainFunctions::getUserSessionFromRequest($request)
                        ],
                        'query' => [
                            'days' => $request->query->get('days'),
                            'endtime' => $request->query->get('endtime'),
                            'subset' => $request->query->get('subset'),
                        ],
                    ],

                );

                $response = new JsonResponse([], 201);
                $response->send();

                $filesystem = new Filesystem();
                $filesystem->mkdir($_ENV['APP_DOWNLOAD_FOLDER'] . $project, 0777);
                $endTimeObject = \DateTime::createFromFormat('Y-m-d H:i:s', $request->query->get('endtime'));


                $headers = $serverResponse->getHeaders(false);

                switch ($serverResponse->getStatusCode()) {
                    case 200:
                        $endTimeString = $endTimeObject->format('Ymd');
                        $filesystem->dumpFile($_ENV['APP_DOWNLOAD_FOLDER'] . $project . '/' . $endTimeObject->sub(new \DateInterval('P'.$request->query->get('days').'D'))->format('Ymd').'-'.$endTimeString. '.csv', $serverResponse->getContent());
                    default:

                }
                return $response;
            } catch (RedirectionExceptionInterface|ClientExceptionInterface|ServerExceptionInterface|TransportExceptionInterface|DecodingExceptionInterface $e) {
                return $response;
            }
        } else
            return CustomerController::notLoggedInResponse($this);
    }

    /**
     * @Route("/data/export/{project}/min-max-date",name="app_data_export_project_date", methods={"GET"})
     */
    public function getMinMaxForTimeSelection(Request $request, string $project): Response
    {
        return MainFunctions::respondGetDataFromBackend($request, $this->backendClient, "/api/data/export/$project/min-max-date");
    }

    /**
     * @Route("/data/export/{project}/history",name="app_data_export_project_history", methods={"GET"})
     */
    public function getHistoricalFiles(Request $request, string $project): Response
    {
        if (CustomerController::hasRights($request, $this->backendClient,$project,true)) {
            try {
                $finder = new Finder();
                $finder->ignoreUnreadableDirs()->files()->in($_ENV['APP_DOWNLOAD_FOLDER']. $project . '/');
                $files = [];
                foreach ($finder as $file) {
                    $files[] = ['filename' => $file->getFilename(), 'date' => date("Y-m-d H:i:s",$file->getMTime())];
                }

                if ($finder->hasResults())
                    return new JsonResponse($files, 200);
                return new JsonResponse([], 204); // NoContent
            } catch (Exception $e) {
                return new JsonResponse([], 204); // NoContent
            }
        } else
            return CustomerController::notLoggedInResponse($this);
    }

    /**
     * @Route("/data/export/{project}/download", name="app_data_export_project_download", methods={"GET"})
     */
    public function downloadFile(Request $request, string $project): Response {
        $fileName = $request->query->get('file');

        if (CustomerController::hasRights($request, $this->backendClient,$project,true)) {
            // Remove anything which isn't a word, whitespace, number
            // or any of the following caracters -_~,;[]().
            // Thanks @Łukasz Rysiak!
            $pathName = preg_replace("([^\w\d\-_\(\).])", '', $fileName);
            // Remove any runs of periods (thanks falstro!)
            $pathName = $_ENV['APP_DOWNLOAD_FOLDER'] . $project . '/'.preg_replace("([\.]{2,})", '', $pathName);

            $response = new BinaryFileResponse($pathName);

            $mimeTypes = new MimeTypes();
            $mimeType = $mimeTypes->guessMimeType($pathName);

            $response->headers->set('Content-Type', $mimeType);

            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $fileName
            );

            return $response;

        } else
            return CustomerController::notLoggedInResponse($this);
    }
}
