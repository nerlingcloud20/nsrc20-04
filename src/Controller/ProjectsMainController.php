<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
;

class ProjectsMainController extends AbstractController
{

    private HttpClientInterface $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }

    /**
     * @Route("/projects",
     *     name="projects_main",
     *     requirements={
     *     "_locale": "en|de",
     *     })
     */
    public function index(Request $request): Response
    {
        $projectData = MainFunctions::getProjectData(MainFunctions::getUserSessionFromRequest($request), $this->backendClient);
        if(sizeof($projectData) === 1)
            return $this->redirectToRoute("projects_project",['project' => $projectData[0]]);
        else
            return $this->render('projects_main/projects.html.twig', [
                'project' => '',
                'menu' => MainFunctions::generateMenuData($request,$this->backendClient),
            ]);
    }

    /**
     * @Route("/projects/{project}",
     *     name="projects_project",
     *     requirements={
     *     "_locale": "en|de",
     *     })
     */
    public function indexWithProject(Request $request,string $project): Response
    {
        return $this->render('projects_main/projects.html.twig', [
            'project' => $project,
            'menu' => MainFunctions::generateMenuData($request,$this->backendClient)
        ]);
    }

    /**
     * @Route("/projects/{project}/possibleHeaders", name="projects_project_possibleHeaders", methods="GET")
     */
    public function getPossibleHeaders(Request $request, string $project): Response {
        try {
            $serverResponse = $this->backendClient->request(
                'GET',
                '/api/projects/'.$project.'/possibleHeaders',
                [
                    'body' => [],
                    'headers' => [
                        'user-session' => MainFunctions::getUserSessionFromRequest($request)
                    ]
                ]
            );

            $headers = $serverResponse->getHeaders(false);

            switch($serverResponse->getStatusCode()) {
                case 200:
                    return new JsonResponse($serverResponse->toArray(true),200);
                case 204:
                    return new JsonResponse(null,204);

                default:
                    return new Response($serverResponse->getContent(false),$serverResponse->getStatusCode(),$headers);
            }
        } catch (RedirectionExceptionInterface | ClientExceptionInterface | ServerExceptionInterface | TransportExceptionInterface | DecodingExceptionInterface $e) {
            return new Response($e,500);
        }
    }

    /**
     * @Route("/projects/{project}/files-to-project",  methods={"GET", "POST"},
     *     name="projects_project_filesToProject",)
     */
    public function getFileNameForPosition(Request $request, string $project): Response
    {
        if($request->isMethod('POST')) {

        }
        return MainFunctions::respondGetDataFromBackend($request,$this->backendClient,"/api/projects/$project/files-to-project");
    }
}
