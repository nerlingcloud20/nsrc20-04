<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CustomviewController extends AbstractController
{

    private HttpClientInterface $backendClient;

    public function __construct(HttpClientInterface $backendClient)
    {
        $this->backendClient = $backendClient;
    }

    /**
     * @Route("/customview/{view}/{hideMenu}", name="app_customview")
     */
    public function index(Request $request, string $view, string $hideMenu): Response
    {
        $projectData = MainFunctions::getProjectData(MainFunctions::getUserSessionFromRequest($request), $this->backendClient);
        $project = in_array(substr($view,0,7),$projectData) ? substr($view,0,7) : '';
        return $this->render('customview/index.html.twig', [
            'project' => $project,
            'customview' => $view,
            'hideMenu' => (bool)$hideMenu,
            'menu' => MainFunctions::generateMenuData($request,$this->backendClient)
        ]);
    }
}
