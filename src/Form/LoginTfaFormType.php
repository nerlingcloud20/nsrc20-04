<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class LoginTfaFormType extends \Symfony\Component\Form\AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', HiddenType::class, [
                'constraints' => new Length(['min' => 5]),
                'label_attr' => ['class' => 'label'],
                'row_attr' => ['class' => 'field tile is-child'],
                'attr' =>  ['class'=>'input',
                    'placeholder' => 'E-Mail']

            ])
            ->add('password', PasswordType::class, [
                'constraints' => new Length(['min' => 5]),
                'label' => 'Password',
                'label_attr' => ['class' => 'label'],
                'row_attr' => ['class' => 'field tile is-child'],
                'attr' =>  ['class'=>'input',
                    'placeholder' => 'Passwort'],
                'icon' => 'fa-key'

            ])
            ->add('Submit', SubmitType::class, [
                'label' => null,
                'row_attr' => ['class' => 'field tile is-child'],
                'attr' => ['class' => 'button is-nsr'],
                'wrapper_class' => 'is-pulled-right'
            ]);
    }

}