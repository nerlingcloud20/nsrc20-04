<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Length;

class LoginFormType extends \Symfony\Component\Form\AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => new Length(['min' => 5]),
                'label' => "E-Mail",
                'label_attr' => ['class' => 'label'],
                'row_attr' => ['class' => 'field tile is-child'],
                'attr' =>  ['class'=>'input',
                    'placeholder' => 'E-Mail'],
                'icon' => 'fa-user'

            ])
            ->add('password', PasswordType::class, [
                'constraints' => new Length(['min' => 5]),
                'label' => new TranslatableMessage('password'),
                'label_attr' => ['class' => 'label'],
                'row_attr' => ['class' => 'field tile is-child'],
                'attr' =>  ['class'=>'input',
                    'placeholder' => new TranslatableMessage('password')],
                'icon' => 'fa-key'

            ])
            ->add('Submit', SubmitType::class, [
                'label' => new TranslatableMessage('login.login'),
                'row_attr' => ['class' => 'field tile is-child'],
                'attr' => ['class' => 'button is-nsr'],
                'wrapper_class' => 'is-pulled-right'
            ]);
    }

}