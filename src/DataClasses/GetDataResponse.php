<?php

namespace App\DataClasses;

class GetDataResponse
{
    public int $httpStatusCode;
    public array $data;
    public bool $isValid;
    public array $headers;
    public string $dataRaw;

    /**
     * @param int $httpStatusCode
     * @param array $data
     * @param bool $isValid
     * @param array $headers
     * @param string $dataRaw
     */
    public function __construct(int $httpStatusCode, array $data, bool $isValid, array $headers, string $dataRaw)
    {
        $this->httpStatusCode = $httpStatusCode;
        $this->data = $data;
        $this->isValid = $isValid;
        $this->headers = $headers;
        $this->dataRaw = $dataRaw;
    }


}