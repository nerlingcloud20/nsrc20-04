<?php

namespace App\DataClasses;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
;

class MenuItem
{

    public string $name;
    public string $link;
    public array $children;
    public bool $selected;


    /**
     * @param string $name
     * @param string $link
     * @param MenuItem $children
     * @param bool $selected
     */
    public function __construct(string $name, string $link, array $children, bool $selected)
    {
        $this->name = $name;
        $this->link = $link;
        $this->children = $children;
        $this->selected = $selected;
    }




}