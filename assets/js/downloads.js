import {createApp} from 'vue';
import Downloads from './pages/AppDownloads';

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import specific icons */
import { fas, faPen, faTrash as fasTrash, faPlus, faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons'

import {
	faFloppyDisk as farFloppyDisk,
	faPenToSquare as farPenToSquare,
	faTrashCan as farTrashCan,
	far
} from "@fortawesome/free-regular-svg-icons";

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* add icons to the library */
library.add(fas, far, faPen, farPenToSquare, fasTrash, farTrashCan,farFloppyDisk, faPlus, faCaretUp, faCaretDown)


import {api} from './api_config'
import Oruga from "@oruga-ui/oruga-next";
import {bulmaConfig} from "@oruga-ui/theme-bulma";


const downloads = createApp(Downloads);
downloads.config.globalProperties.$filters = {
	str_limit(value, size, showDots) {
		if (!value) return '';

		if (value.length <= size) {
			return value;
		}
		return value.substring(0, size) + (showDots ? '...' : '');
	}
}
downloads
	.use(Oruga, bulmaConfig)
	.provide('$api',api) // Providing to all components during app creation
	.provide('$str_limit',(value, size, showDots) => {
		if (!value) return '';
		if (value.length <= size) {
			return value;
		}
		return value.substring(0, size) + (showDots ? '...' : '');
	})
	.component("FontAwesomeIcon", FontAwesomeIcon)
	.mount('#downloads')
