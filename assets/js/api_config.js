import {useCookies} from "@vueuse/integrations/useCookies";

let api_headers_loggedIn = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'user-session': useCookies().get('user-session')
}

let unauthorizedRedirect = () => {
	window.location.href = '/login';
}

export const api = {
	endpoint: '',//https://dev01.nerling.cloud:8000',
	get: {
		method: 'GET',
		headers: api_headers_loggedIn
	},
	headers_loggedIn: api_headers_loggedIn,
	promisedResponse: (response,failFunction) => {
		if(response.status === 200)
			return response.json();
		else if(response.status === 401) {
			unauthorizedRedirect();
			return [];
		}
		else {
			failFunction();
			return [];
		}
	},
	put_post: (endpoint,data,httpFunction,successFunction) => {
		const apiOptions = {
			method: httpFunction,
			headers: api_headers_loggedIn,
			body: JSON.stringify({ ...data})
		}

		fetch(endpoint.value, apiOptions)
			.then(response => {
				if(!response.ok) {
					const error = (response && response.message) || response.status;
					return Promise.reject(error)
				} else {
					successFunction()
					return response.json()
				}
			})
			// eslint-disable-next-line no-console
			.catch(error => {console.error('There was an error!', error);});
	},
	unauthorized: () => {
		unauthorizedRedirect();
	},
	fetch: (uri) => {
		return fetch(uri, api.get)
			.then(response => {
				if(response.status === 200)
					return response.json()
				else
					return []
			})
	},
	call: (uri) => {
		return fetch(uri, api.get)
	}
}