import {createApp} from 'vue';
import Project from './pages/AppProjects';

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import specific icons */
import { fas, faTrash as fasTrash, faCheck, faXmark, faPlus, faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons'

import { far, faPenToSquare as farPenToSquare, faTrashCan as farTrashCan, faFloppyDisk as farFloppyDisk } from '@fortawesome/free-regular-svg-icons'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* add icons to the library */
library.add(fas, far, farPenToSquare, fasTrash, farTrashCan, faCheck, faXmark, farFloppyDisk, faPlus, faAngleDown, faAngleUp)

import {api} from './api_config'

const projects = createApp(Project);

projects.config.globalProperties.$filters = {
	str_limit(value, size, showDots) {
		if (!value) return '';

		if (value.length <= size) {
			return value;
		}
		return value.substring(0, size) + (showDots ? '...' : '');
	}
}

projects
	.provide('$api',api) // Providing to all components during app creation
	.component("FontAwesomeIcon", FontAwesomeIcon)
	.mount('#projects')
