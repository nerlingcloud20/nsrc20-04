"use strict";

import moment from "moment";

export class QualityControlRule {
	constructor(project,type,sensor1,sensor2,value1,value2,distance,active, id, unit, current) {
		this.project = project;
		this.type = type;
		this.sensor1 = sensor1;
		this.sensor2 = sensor2;
		this.value1 = value1;
		this.value2 = value2;
		this.distance = distance;
		this.active = active;
		this.id = id;
		this.unit = unit;
		this.current = QualityControlData.getFromElement(current);
		this.stopLightRuleObject = null;
		this.edit = false;
	}
	static get(project,type,sensor1,sensor2,value1,value2,distance,active, id, unit, current) {
		return new QualityControlRule(project,type,sensor1,sensor2,value1,value2,distance,active,id, unit, current);
	}
	static getFromElement(element) {
		if(element['rule'] === null || element['rule'] === undefined)
			return new QualityControlRule(element.project,element.type,element.sensor1,element.sensor2,element.value1,element.value2,element.distance,element.active, element.id, element.unit, null);
		else
			return new QualityControlRule(element.rule.project,element.rule.type,element.rule.sensor1,element.rule.sensor2,element.rule.value1,element.rule.value2,element.rule.distance,element.rule.active, element.rule.id, element.rule.unit, element.data);
	}

/*	static refreshRules(elements) {
		let ids = "";
		elements.forEach(elements => {

		})
	}
*/
	getStopLightRuleObject() {
		if(this.stopLightRuleObject == null) {
			this.stopLightRuleObject = StopLightRuleObject.createNew(this);
		}
		return this.stopLightRuleObject
	}

	checkForGK_1H() {
		let diff = this.current.upperValue - this.current.lowerValue;
		if(this.type.startsWith("GKS") && this.value1 < 0.19 && diff < this.value1) {
			return 0;
		}
		if(diff < 0.25)
			return 1;
		if(diff < 0.45)
			return 2;
		if(diff < 1.05)
			return 3;
		if(diff < 2.05)
			return 4;
		return 9;
	}
	checkForGK_24H() {
		let diff = this.current.upperValue - this.current.lowerValue;
		if(this.type.startsWith("GKS") && this.value1 < 0.39 && diff < this.value1) {
			return 0;
		}
		if(diff < 0.45)
			return 1;
		if(diff < 0.85)
			return 2;
		if(diff < 2.05)
			return 3;
		if(diff < 3.05)
			return 4;
		return 9;
	}
	checkForGK_D() {
		let diff = this.current.value;
		if(this.type.startsWith("GKS") && this.value1 < 0.19 && diff < this.value1) {
			return 0;
		}
		if(diff < 0.25)
			return 1;
		if(diff < 0.35)
			return 2;
		if(diff < 0.55)
			return 3;
		if(diff < 1.05)
			return 4;
		return 9;
	}
	getBorderForStopLightFromType() {
		if(this.type.startsWith("GKS"))
			return this.value1;
		if(this.type === "MIN_MAX")
			return Array.of(this.value1,this.value2);
		if(this.type.endsWith("D")) {
			if(this.type === "GK1_D")
				return 0.2;
			if(this.type === "GK2_D")
				return 0.3;
			if(this.type === "GK3_D")
				return 0.5;
			if(this.type === "GK4_D")
				return 1.0;
		}
		if(this.type.endsWith("1H")) {
			if(this.type === "GK1_1H")
				return 0.2;
			if(this.type === "GK2_1H")
				return 0.4;
			if(this.type === "GK3_1H")
				return 1.0;
			if(this.type === "GK4_1H")
				return 2.0;
		}
		if(this.type.endsWith("24H")) {
			if(this.type === "GK1_24H")
				return 0.4;
			if(this.type === "GK2_24H")
				return 0.8;
			if(this.type === "GK3_24H")
				return 2.0;
			if(this.type === "GK4_24H")
				return 3.0;
		}
		return 100.0;

	}
	gK_Target() {
		return QualityControlTypeName[this.type];
	}
	gK_Current() {
		if (this.isDistance()) {
			return this.checkForGK_D();
		}
		if (this.isHour()) {
			return this.checkForGK_1H();
		}
		if (this.isDay()) {
			return this.checkForGK_24H();
		}
	}
	isDay() {
		return this.type.endsWith("_24H")
	}
	isHour() {
		return this.type.endsWith("_1H")
	}
	isDistance() {
		return this.type.endsWith("_D")
	}
	isMinMax() {
		return this.type === "MIN_MAX";
	}
	getTypeUndetailed() {
		if(this.isDay())
			return "DAY";
		if(this.isHour())
			return "HOUR"
		if(this.isDistance())
			return "DIST"
		if(this.isMinMax())
			return "MIN_MAX"
	}
	updateCurrent(data) {
//		this.getStopLightRuleObject().updateCurrent(data); do that manually so the values get updated (because reactive)
		this.current.updateCurrent(data);
	}
}

export class QualityControlData {
	constructor(qcrId, value, lowerValue, upperValue, time) {
		this.id = qcrId;
		this.value = value;
		this.lowerValue = lowerValue;
		this.upperValue = upperValue;
		this.time = time;
	}
	static getFromElement(element) {
		if(element === null)
			return null
		else
			return new QualityControlData(element.qcrId, element.value, element.lowerValue, element.upperValue, moment.utc(element.time).local());
	}
	updateCurrent(data) {
		this.value = data.value;
		this.lowerValue = data.lowerValue;
		this.upperValue = data.upperValue;
		this.time = moment.utc(data.time).local();
	}

}

export class StopLightRuleObject {
	constructor(name, current, target, unit, rule) {
		this.name = name;
		this.current = current;
		this.target = target;
		this.unit = unit;
		this.rule = rule;
		this.precission = Math.max(1,StopLightRuleObject.countDecimals(target));
	}
	static createNew(rule) {
		if(rule.type.endsWith("_D"))
			return new StopLightRuleObject(`[${rule.sensor1}<->${rule.sensor2}]`,rule.current.value,rule.getBorderForStopLightFromType(),rule.unit,rule);
		if(rule.type === "MIN_MAX")
			return new StopLightRuleObject(rule.sensor1,rule.current.value,rule.getBorderForStopLightFromType(),rule.unit,rule);
		return new StopLightRuleObject(rule.sensor1,rule.current.value,rule.getBorderForStopLightFromType(),rule.unit,rule);
	}
	static updateData(reactiveObject, newData) {
		reactiveObject.current = newData.value;
		reactiveObject.rule.current.lowerValue = newData.lowerValue;
		reactiveObject.rule.current.upperValue = newData.upperValue;
		reactiveObject.rule.current.value = newData.value;
		reactiveObject.rule.current.time = moment.utc(newData.time).local();
		reactiveObject.rule.stopLightRuleObject.current = newData.value;
	}
	updateCurrent(data) {
		this.current = data.value;

	}
	static countDecimals(value) {
		if(Array.isArray(value)) {
			let largest = 0;
			return value.forEach(function(elem){
				if(elem > largest)
					largest = elem;
			});
		}
		if (Math.floor(value) !== value)
			return (value.toString()).split(".")[1].length || 0;
		return 0;
	}
	roundToPrecission() {
		var number = this.current + Number.EPSILON;
		return number.toFixed(this.precission);
	}

	isInSpecification() {
		if(Array.isArray(this.target)) {
			return this.roundToPrecission() >= (this.target[0]+Number.EPSILON) && this.roundToPrecission() <= (this.target[1]+Number.EPSILON);
		} else
			return this.roundToPrecission() <= (this.target+Number.EPSILON);
	}
}
/*
const QualityControlType = Object.freeze({
	GK1_D: 0,
	GK1_1H: 1,
	GK1_24H: 2,
	GK2_D: 3,
	GK2_1H: 4,
	GK2_24H: 5,
	GK3_D: 6,
	GK3_1H: 7,
	GK3_24H: 8,
	GK4_D: 9,
	GK4_1H: 10,
	GK4_24H: 11,
	GKS_D: 12,
	GKS_1H: 13,
	GKS_24H: 14,
	MIN_MAX: 15
});
*/
const QualityControlTypeName = Object.freeze({
	GK1_D: "1",
	GK1_1H: "1",
	GK1_24H: "1",
	GK2_D: "2",
	GK2_1H: "2",
	GK2_24H: "2",
	GK3_D: "3",
	GK3_1H: "3",
	GK3_24H: "3",
	GK4_D: "4",
	GK4_1H: "4",
	GK4_24H: "4",
	GKS_D: "5",
	GKS_1H: "5",
	GKS_24H: "5",
	MIN_MAX: 15
});