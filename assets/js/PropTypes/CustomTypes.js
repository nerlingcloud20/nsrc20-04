'use strict';
import {api} from '../api_config'
export class StopLightDataObject {
	constructor(name, current, target, unit) {
		this.name = name;
		this.current = current;
		this.target = target;
		this.unit = unit;
		this.precission = StopLightDataObject.countDecimals(target);
	}
	static get(name, current, target, unit) {
		return new StopLightDataObject(name, current, target, unit);
	}
	static countDecimals(value) {
		if(Array.isArray(value)) {
			let largest = 0;
			return value.forEach(function(elem){
				if(elem > largest)
					largest = elem;
			});
		}
		if (Math.floor(value) !== value)
			return (value.toString()).split(".")[1].length || 0;
		return 0;
	}
	roundToPrecission() {
		var number = this.current + Number.EPSILON;
		return number.toFixed(this.precission);
	}

	isInSpecification() {
		if(Array.isArray(this.target)) {
			return this.roundToPrecission() >= (this.target[0]+Number.EPSILON) && this.roundToPrecission() <= (this.target[1]+Number.EPSILON);
		} else
			return this.roundToPrecission() <= (this.target+Number.EPSILON);
	}
}
export class UserDataObject {
	constructor(id,name,email,used2fa) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.used2fa = used2fa;
	}

	static get(obj) {
		return new UserDataObject(obj.id, obj.name, obj.email, obj.used2fa);
	}
}
export class ProjectFile{
	constructor(project, fileType, fileName, dateAdded) {
		this.project = project;
		this.fileType = fileType;
		this.fileName = fileName;
		this.dateAdded = dateAdded;
	}
	static get(obj) {
		return new ProjectFile(obj.project, obj.fileType, api.endpoint+'/'+obj.fileName, obj.dateAdded);
	}
}

export class DownloadBuilder {
	constructor(project, selectedValues, selectedTimeframe, detailed) {
		this.project = project;
		this.selectedValues = selectedValues;
		this.selectedTimeframe = selectedTimeframe;
		this.detailed = detailed;
	}
}