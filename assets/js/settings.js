import {createApp} from 'vue';
import Settings from './pages/AppSettings';

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import specific icons */
import { fas, faUser, faEnvelope, faKey, faPen, faTrash as fasTrash, faCheck, faXmark, faPlus, faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons'

import {
	faFloppyDisk as farFloppyDisk,
	faPenToSquare as farPenToSquare,
	faTrashCan as farTrashCan
} from "@fortawesome/free-regular-svg-icons";

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


/* add icons to the library */
library.add(fas, faUser, faEnvelope, faKey, faPen, farFloppyDisk, farPenToSquare, fasTrash, farTrashCan, faCheck, faXmark, farFloppyDisk, faPlus, faAngleDown, faAngleUp)

import Oruga from '@oruga-ui/oruga-next'
import { bulmaConfig } from '@oruga-ui/theme-bulma'



import {api} from './api_config'


const settings = createApp(Settings);
settings.config.globalProperties.$filters = {
	str_limit(value, size, showDots) {
		if (!value) return '';

		if (value.length <= size) {
			return value;
		}
		return value.substring(0, size) + (showDots ? '...' : '');
	}
}
settings
	.use(Oruga, bulmaConfig)
	.provide('$api',api) // Providing to all components during app creation
	.provide('$str_limit',(value, size, showDots) => {
		if (!value) return '';
		if (value.length <= size) {
			return value;
		}
		return value.substring(0, size) + (showDots ? '...' : '');
	})
	.component("FontAwesomeIcon", FontAwesomeIcon)
	.mount('#settings')
